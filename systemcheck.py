"""
System check to make sure that you can run most of the required code

Note: there are some questionable programming practices evident here!
"""

# first check for numpy
import numpy as np
import matplotlib.pyplot as plt
import scipy as sp

# plotting
# Note: the form here is the procedural MATLAB-style plotting.
# You can also use the more object-oriented form but it's less common.
alpha = np.linspace(-np.pi, np.pi, 512)
# hints:
# don't build arrays manually - use linspace as above
# similarly, use builtin constants!

plt.figure()
plt.plot(alpha, np.cos(alpha), label='cos', linewidth=2.5)
plt.plot(alpha, np.sin(alpha), label='sin', linewidth=2.5)
plt.title('Test graph plot in 2D')
plt.ylabel('Value [a.u.]')
plt.xlabel('Angle [rad]')
plt.legend()
plt.grid()
plt.show()

# interpolation, fft use the numerical libraries:
# a good "smoke test" to see if scipy is actually working.
import scipy.fftpack as fftpack
plt.figure()
plt.plot(np.abs(fftpack.fftshift(fftpack.fft(np.cos(alpha))))/256, label='cos')
# can you add in the sine function's FFT?
# can you title this graph?
# can you add a legend box to show the labels?
# can you turn on the grid
plt.show()


# http://docs.scipy.org/doc/scipy/reference/tutorial/interpolate.html
from scipy.interpolate import interp1d
x = np.linspace(0, 10, num=11, endpoint=True)
y = np.cos(-x**2/9.0)
f = interp1d(x, y)
f2 = interp1d(x, y, kind='cubic')
xnew = np.linspace(0, 10, num=41, endpoint=True)
plt.figure()
plt.plot(x, y, 'o', xnew, f(xnew), '-', xnew, f2(xnew), '--')
plt.legend(['data', 'linear', 'cubic'], loc='best')
plt.show()


