Aim
===

The purpose of this lab exercise is to ascertain that you've the
principal parts of the required software to undertake this module.
*Later on, we'll need to install some additional bits.*

Procedures
==========

Installing git
--------------

The first part of this lab is actually concerned with getting the
required source code. In this course we will use the Git version control
system, initally just to distribute sample and starting code. Later on,
you'll submit your coding work using it.

If you're running on Mac/Linux, you might have ``git`` installed
already. If you're already using git, move straight along to the next
section. To see if you do, open a terminal and run the ``git`` command.
If it prints a help message, then you're all set.

If you don't see Git:

-  **Mac users:** install XCode from the App Store and run it once. That
   will set up Git.
-  **Linux users:** install using your package manager (e.g.
   ``sudo apt-get install git``)

Windows users are recommended to install the normal command-line version
of Git. If you've a Git program in your start menu, then you're all set.
Run the Git shell to be sure.

Cloning (a.k.a. downloading) the source code
--------------------------------------------

You should make a directory to store your work in this module, in a
location easily-accessible from the command line. Then in the terminal
(or git shell for Windows), change into this directory. Use the HTTPS or
SSH link of the repository to clone it to your own computer, using the
command shown. Once the clone operation has completed, you'll find a
number of files (including this one!) in a newly-created folder.

/Please don't just use the download ZIP link above! The aim of this
section is to make sure you actually have git working./

Python check
------------

Run the suppiled `systemcheck.py <file:systemcheck.py>`__ file to make
sure that your Python installation works.

It will do a basic demonstration of some of the capabilities of numpy,
scipy and matplotlib.

If you're missing dependencies (i.e. if the import statements fail) just
try to install them and try again.

Once the file runs without error, you have a suitable Python
installation working.

C++ check (optional)
--------------------

You can attempt to compile and run the
`systemcheck.cpp <file:systemcheck.cpp>`__ file.

LaTeX document
--------------

You can attempt to compile and view the PDF produced by the
`sample.tex <file:sample.tex>`__ file.

If on Windows, MiKTeX is a good option.
